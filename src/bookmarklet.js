 
window.serializeWithStyles = function()
   {// Mapping between tag names and css default values lookup tables. This allows to exclude default values in the result.
   var defaultStylesByTagName = {};// Styles inherited from style sheets will not be rendered for elements with these tag names
    var noStyleTags =  {"BASE": true, "HEAD": true, "HTML": true, "META": true, "NOFRAME": true, "NOSCRIPT": true, "PARAM": true, "SCRIPT": true, 
      "STYLE": true, "TITLE": true};// This list determines which css default values lookup tables are precomputed at load time
      // Lookup tables for other tag names will be automatically built at runtime if needed
    var tagNames =["A", 
      "ABBR", 
      "ADDRESS", 
      "AREA", 
      "ARTICLE", 
      "ASIDE", 
      "AUDIO", 
      "B", 
      "BASE", 
      "BDI", 
      "BDO", 
      "BLOCKQUOTE", 
      "BODY", 
      "BR", 
      "BUTTON", 
      "CANVAS", 
      "CAPTION", 
      "CENTER", 
      "CITE", 
      "CODE", 
      "COL", 
      "COLGROUP", 
      "COMMAND", 
      "DATALIST", 
      "DD", 
      "DEL", 
      "DETAILS", 
      "DFN", 
      "DIV", 
      "DL", 
      "DT", 
      "EM", 
      "EMBED", 
      "FIELDSET", 
      "FIGCAPTION", 
      "FIGURE", 
      "FONT", 
      "FOOTER", 
      "FORM", 
      "H1", 
      "H2", 
      "H3", 
      "H4", 
      "H5", 
      "H6", 
      "HEAD", 
      "HEADER", 
      "HGROUP", 
      "HR", 
      "HTML", 
      "I", 
      "IFRAME", 
      "IMG", 
      "INPUT", 
      "INS", 
      "KBD", 
      "KEYGEN", 
      "LABEL", 
      "LEGEND", 
      "LI", 
      "LINK", 
      "MAP", 
      "MARK", 
      "MATH", 
      "MENU", 
      "META", 
      "METER", 
      "NAV", 
      "NOBR", 
      "NOSCRIPT", 
      "OBJECT", 
      "OL", 
      "OPTION", 
      "OPTGROUP", 
      "OUTPUT", 
      "P", 
      "PARAM", 
      "PRE", 
      "PROGRESS", 
      "Q", 
      "RP", 
      "RT", 
      "RUBY", 
      "S", 
      "SAMP", 
      "SCRIPT", 
      "SECTION", 
      "SELECT", 
      "SMALL", 
      "SOURCE", 
      "SPAN", 
      "STRONG", 
      "STYLE", 
      "SUB", 
      "SUMMARY", 
      "SUP", 
      "SVG", 
      "TABLE", 
      "TBODY", 
      "TD", 
      "TEXTAREA", 
      "TFOOT", 
      "TH", 
      "THEAD", 
      "TIME", 
      "TITLE", 
      "TR", 
      "TRACK", 
      "U", 
      "UL", 
      "VAR", 
      "VIDEO", 
      "WBR"];// Precompute the lookup tables.
    for (var i = 0;i < tagNames.length;i++)
       {if ( ! noStyleTags[tagNames[i]])
           {defaultStylesByTagName[tagNames[i]] = computeDefaultStyleByTagName(tagNames[i]);
           }
       }
   
    function computeDefaultStyleByTagName(tagName)
       {var defaultStyle = {};
        var element = document.body.appendChild(document.createElement(tagName));
        var computedStyle = getComputedStyle(element);
        for (var i = 0;i < computedStyle.length;i++)
           {defaultStyle[computedStyle[i]] = computedStyle[computedStyle[i]];
           }
        document.body.removeChild(element);
        return defaultStyle;
       }
   
    function getDefaultStyleByTagName(tagName)
       {tagName = tagName.toUpperCase();
        if ( ! defaultStylesByTagName[tagName])
           {defaultStylesByTagName[tagName] = computeDefaultStyleByTagName(tagName);
           }
        return defaultStylesByTagName[tagName];
       }
    return function serializeWithStyles()
       {if (this.nodeType !== Node.ELEMENT_NODE)
           {throw new TypeError();
           }
        var cssTexts =[];
        var elements = this.querySelectorAll( "*");
        for (var i = 0;i < elements.length;i++)
           {var e = elements[i];
            if ( ! noStyleTags[e.tagName])
               {var computedStyle = getComputedStyle(e);
                var defaultStyle = getDefaultStyleByTagName(e.tagName);
                cssTexts[i] = e.style.cssText;
                for (var ii = 0;ii < computedStyle.length;ii++)
                   {var cssPropName = computedStyle[ii];
                    if (computedStyle[cssPropName] !== defaultStyle[cssPropName])
                       {e.style[cssPropName] = computedStyle[cssPropName];
                       }
                   }
               }
           }
        var result = this.outerHTML;
        for (var i = 0;i < elements.length;i++)
           {elements[i].style.cssText = cssTexts[i];
           }
        return result;
       }.call(this);
   };/**!
 * Element Picker.
 * A JavaScript library that allows you to point and click to get the hovered element.
 * @author  James Bechet <jamesbechet@gmail.com>
 * @license MIT
 */
(function elementPickerModule(factory)
   {if (typeof define === 'function' && define.amd)
       {define(factory);
       }
    else if (typeof module != 'undefined' && typeof module.exports != 'undefined')
       {module.exports = factory();
       }
    else 
       {window['elementPicker'] = factory();
       }
   })(function elementPickerFactory()
   {if (typeof window === 'undefined' || ! window.document)
       {console.error('elementPicker requires the window and document.');
       }
    var oldTarget;
    var desiredBackgroundColor = 'rgba(0, 0, 0, 0.1)';
    var oldBackgroundColor;
    var onClick;
   
    function onMouseMove(event)
       {event = event || window.event;
        var target = event.target || event.srcElement;
        if (oldTarget)
           {           resetOldTargetColor();
           }
        else 
           {document.body.style.cursor = 'pointer';
           }
        oldTarget = target;
        oldBackgroundColor = target.style.backgroundColor;
        target.style.backgroundColor = desiredBackgroundColor;
       }
   
    function onMouseClick(event)
       {event = event || window.event;
        var target = event.target || event.srcElement;
        if (event.preventDefault) event.preventDefault();
        if (event.stopPropagation) event.stopPropagation();
        onClick(target);
        reset();
        return false;
       }
   
    function reset()
       {document.removeEventListener('click', onMouseClick, false);
        document.removeEventListener('mousemove', onMouseMove, false);
        document.body.style.cursor = 'auto';
        if (oldTarget)
           {resetOldTargetColor();
           }
        oldTarget = null;
        oldBackgroundColor = null;
       }
   
    function resetOldTargetColor()
       {oldTarget.style.backgroundColor = oldBackgroundColor;
       }
   
    function init(options)
       {if ( ! options || ! options.onClick)
           {console.error('onClick option needs to be specified.');
            return;
           }
        desiredBackgroundColor = options.backgroundColor || desiredBackgroundColor;
        onClick = options.onClick;
        document.addEventListener('click', onMouseClick, false);
        document.addEventListener('mousemove', onMouseMove, false);
        return elementPicker;
       }
   /**
   * The library object.
   * @property {Function} init    - Function called to init the library.
   * @property {Function} onClick - The callback triggered once an element is clicked.
   * @property {String} version   - The library's version.
   * @type {Object}
   */
    var elementPicker = {};
    elementPicker.version = '1.0.1';
    elementPicker.init = init;
    return elementPicker;
});


function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  
  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}


function generate(el){
	  setTimeout( function(){
       copyTextToClipboard( serializeWithStyles.call(el) );
       alert("HTML copied to clipboard");
     },200 )   
}


alert("use the mouse to select an element");
elementPicker.init({onClick: generate });
